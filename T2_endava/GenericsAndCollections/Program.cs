﻿using System;

namespace GenericsAndCollections
{
    class Program
    {
        
        static void Main(string[] args)
        {
            //var intGenericClass = new GenericClass<int>();
            //var stringGeneric = new GenericClass<string>();
            //var animalGeneric = new GenericClass<Animal>();
            var dogGeneric = new GenericClass<Dog>();
            //var catGeneric = new GenericClass<Cat>();
            var dog = new Dog();
           
        }
    }

    
    public class GenericClass<T> 
        where T : class, ICloneable, IComparable, new()
    {
        public T Field { get; set; }
    }
    public class Animal 
    {
        public ICloneable Memeber { get; set; }

        public string Name { get; set; }

        public void MethodWhichUsesClone()
        {
            Memeber.Clone();
        }
        
    }
    public class Dog : Animal, ICloneable, IComparable
    {
        public Dog()
        {
            
        }
        public Dog(string input)
        {
            this.Name = input;
        }
        public object Clone()
        {
            throw new NotImplementedException();
        }

        public int CompareTo(object? obj)
        {
            throw new NotImplementedException();
        }
    }

    public class Cat : Animal, ICloneable
    {
        public object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
