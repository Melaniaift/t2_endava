﻿//3.Use a similar collection to list that doesn’t allow duplicate values, populate it with values(try adding duplicate 
//values) and display it.
using System;
using System.Collections.Generic;

namespace Tema2ex3
{
    class Program
    {
        static void Main(string[] args)
        {
            HashSet<string> no_duplicates = new HashSet<string>();
            no_duplicates.Add("A");
            no_duplicates.Add("B");
            no_duplicates.Add("C");
            no_duplicates.Add("C");
            no_duplicates.Add("D");
            foreach (var val in no_duplicates)
            {
                Console.WriteLine(val);
            }
      
        }
    }
}
