﻿//1.Create a generic method that receives as input an array and returns it in reverse. Display at console the reverse array.
using System;

namespace Tema2ex1
{
    class Program
    {
        public static void Change<T>(ref T[] array)
        {
            T temp;
            for (int i = 0; i < array.Length / 2; i++)
            {
                temp = array[i];
                array[i] = array[array.Length - i - 1];
                array[array.Length - i - 1] = temp;
            }
        }
        static void Main(string[] args)
        {
            int[] array = { 0, 1, 2, 3, 4 };
            Change<int>(ref array);
            Console.WriteLine(string.Join(",", array));
            
        }
    }
}
