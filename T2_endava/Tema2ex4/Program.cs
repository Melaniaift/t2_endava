﻿/*4.Create a Customer and an Order class(with some specific properties e.g. Name, Age, Date). One Customer can have multiple 
Orders.Create a collection of Customers with a collection of Orders(min 3 customers, each having a few orders).
Iterate through each customer’s order and store them in a Key/Value collection having the Customer name as key and 
as value all orders specific for that customer. Display the Key/Value collection.*/
using System;
using System.Collections.Generic;

namespace Tema2ex4
{
    public class Customer {
        public string Name;
        public int Age;
        public HashSet<string> orders = new HashSet<string>();
    }
    public class Order {
        public string orderNr;
        public DateTime data;
    }
    class Program
    {
        static void Main(string[] args)
        {
            
            Customer C1 = new Customer();
            C1.Name = "Ana";
            C1.Age = 18;
            C1.orders.Add("a");
            C1.orders.Add("a");
            C1.orders.Add("b");
            
            foreach (var val1 in C1.orders)
            {
                Console.WriteLine(val1);
            }
        }
    }
}
