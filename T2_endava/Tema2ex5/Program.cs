﻿/*5.For the previous created models create a generic interface that expose the CRUD (create-read-update-delete) 
operations for those entities(repository). For one implementation of the repository store the values into a simple 
collection and for the other one store them using a key/value collection in order to be able to group them by 
a common information.*/
using System;

namespace Tema2ex5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
