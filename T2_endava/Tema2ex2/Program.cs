﻿//2.Create a class that has two generic parameters(one that have the constraint of being a reference type and the 
//constructor constraint and the other one to implement a generic interface created by you).Create a method inside the 
//class that displays the types of both parameters.
using System;

namespace Tema2ex2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ClassExample Obj1 = new ClassExample();
            ImplementationClass Obj2 = new ImplementationClass();
            var exGeneric = new ExampleGenericClass<ClassExample, ImplementationClass>();
            
            var p = exGeneric.Show<ClassExample, ImplementationClass>(Obj1, Obj2);
            Console.WriteLine(p);
        }
    }
}
