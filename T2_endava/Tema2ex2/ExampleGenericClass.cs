﻿//2.Create a class that has two generic parameters(one that have the constraint of being a reference type and the 
//constructor constraint and the other one to implement a generic interface created by you).Create a method inside the 
//class that displays the types of both parameters.

namespace Tema2ex2
{
    public class ExampleGenericClass<T1,T2>
        where T1 : class, new()
        where T2 : ISampleInterface //generic interface created by me
    {
        public T1 SomeVar = new T1(); //constructor constraint

        public T2 OtherVar;

        public string Show<T1, T2>(T1 x, T2 y)
        {
            string string1 = typeof(T1) + " " + typeof(T2);
            return string1;
        }
    }
}
