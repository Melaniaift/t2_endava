﻿//2.Create a class that has two generic parameters(one that have the constraint of being a reference type and the 
//constructor constraint and the other one to implement a generic interface created by you).Create a method inside the 
//class that displays the types of both parameters.

namespace Tema2ex2
{
    public class ImplementationClass : ISampleInterface
    {
        void ISampleInterface.SampleMethod(){}
    }
}
